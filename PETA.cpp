#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <limits>
using namespace std;

int n = 22, answer, computer, collectedMatches = 0, computerCollectedMatches = 0;
char prompt;

void ignoreLine() {
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

int checkNumber(int &number, int leftoverPile, int &collectedMatches) {
	while(true) {
	if (std::cin.fail()) { // if the first character is not a valid data type, cin.clear(); and ignoreLine();
		std::cin.clear();
		ignoreLine();
		cout << "Invalid input. Would you like to try again? y/n: ";
		cin >> prompt;
		if (prompt == 'y' || prompt == 'Y') { // if yes, try again until number is from 1 to 4
			do {
				cout << "Please pick again: ";
				cin >> number;
				if (std::cin.fail()) {
					std::cin.clear();
				}
				ignoreLine();
			} while (number > 4 || number < 1);
			collectedMatches += number;
			return number & collectedMatches;
		} else {
			std::exit(0);
		}
	  }	else if (leftoverPile == 0) {
	  		return number = 0;
	  } else if(number > 4 || number < 1) { // checks if number is greater than four or lesser than 1
		cout << "Invalid input. Would you like to try again? y/n: ";
		ignoreLine();
		cin >> prompt;
		if (prompt == 'y' || prompt == 'Y') { // if yes, try again until number is from 1 to 4
			do {
				cout << "Please pick again: ";
				cin >> number;
				if (std::cin.fail()) {
					std::cin.clear();
				}
				ignoreLine();
			} while (number > 4 || number < 1);
			collectedMatches += number;
			return number & collectedMatches;
		} else {
			std::exit(0);
		}
	  }	else if (number > leftoverPile) { // if number is greater than the pile, then try again until number's between from 1 to 4
	  	cout << "Invalid input. Would you like to try again? y/n: ";
		cin >> prompt;
		 if (prompt == 'y' || prompt == 'Y') { // if yes, try again until number is from 1 to 4
			do {
				cout << "Please pick again: ";
				cin >> number;
				if (std::cin.fail()) {
					std::cin.clear();
				}
				ignoreLine();
			} while (number > leftoverPile || number < 1 || number > 4);
			collectedMatches += number;
			return number & collectedMatches;
	    } else {
	    	std::exit(0);
		}
	   } else {
	  	collectedMatches += number;
		return number & collectedMatches;
	  }
	}
}

int computersChoice(int leftoverPile) {
	srand (time(NULL));
	if (leftoverPile <= 4 && !(leftoverPile <= 0)) { // if pile is lesser than or equal to 4 and pile is not lesser than or equal to 0
	  computer = n;
	  return computer;
	} else if (leftoverPile <= 0) { // if pile is lesser than or equal to 0
		return computer = 0;
	} else if (leftoverPile > 4) { // if pile is greater than 4
		computer = n % ((0x3^0x9)>>1); // AI strategy
		if (computer == 0) { // computer's remainder is 0
			computer = rand() %4 + 1; // random number from 1 to 4
		} else {
			return computer;
		}
	}
}

int main() {
	cout << "Hello and Welcome to the Game of Nim!" << endl;
	cout << "Would you like to go first? y/n: ";
	cin >> prompt;
	ignoreLine();
	
	srand(time(NULL)); // intialized seed
	
	if (prompt == 'y' || prompt == 'Y') {
   	 do {
   	 	if(n == 0) {
   	 		cout << "computer wins!";
   	 		break;	
		}
   	 	cout << endl << "Leftover pile: " << n << endl;
   	 	for (int count = n; count > 0; count--) { 
   	 		cout << "|";
		}
		cout << endl << "Your matches: " << collectedMatches << "\tComputer's Matches: " << computerCollectedMatches;
   	 	cout << endl << "Pick from the pile: ";
   	 	cin >> answer;
   	 	checkNumber(answer, n, collectedMatches);
   	 	n -= answer;
   	 	if(n == 0) {
   	 		cout << "User Wins!" << endl << endl;
   	 		break;	
		}
   	 	cout << endl << "Leftover pile: " << n << endl;
   	 	cout << "Computer's pick: ";
   	 	computersChoice(n);
   	 	cout << computer << endl;
   	 	computerCollectedMatches += computer;
   	 	n -= computer;
   	 	if(n == 0) {
   	 		cout << "Computer Wins!" << endl << endl;
			break;	
		}
	 } while(n >= 1);
	} else if (prompt == 'n' || prompt == 'N') {
		do {
   	 	cout << endl << "Leftover pile: " << n << endl;
   	 	cout << "Computer's pick: ";
   	 	computersChoice(n);
   	 	cout << computer << endl;
   	 	computerCollectedMatches += computer;
   	 	n -= computer;
   	 	if(n == 0) {
   	 		cout << "Computer Wins!" << endl << endl;
			break;	
		}
   	 	cout << endl << "Leftover pile: " << n << endl;
   	 	for (int count = n; count > 0; count--) { 
   	 		cout << "|";
		}
		cout << endl << "Your matches: " << collectedMatches << "\tComputer's Matches: " << computerCollectedMatches;
   	 	cout << endl << "Pick from the pile: ";
   	 	cin >> answer;
   	 	checkNumber(answer, n, collectedMatches);
   	 	n -= answer;
	 } while(n >= 1);
	} else {
		std::exit(0);
	}
	
	system("pause");
	return 0;
}
